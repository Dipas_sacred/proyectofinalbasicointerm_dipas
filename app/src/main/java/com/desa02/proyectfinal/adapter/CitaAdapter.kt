package com.desa02.proyectfinal.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.desa02.proyectfinal.R
import com.desa02.proyectfinal.databinding.ItemCitaBinding
import com.desa02.proyectfinal.model.Cita

class CitaAdapter(var citas: MutableList<Cita> = mutableListOf(), val callback: (item:Cita)-> Unit)
    : RecyclerView.Adapter<CitaAdapter.CitaAdapterViewHolder>() {
    inner class CitaAdapterViewHolder( itemView: View ): RecyclerView.ViewHolder(itemView){

        private val binding:ItemCitaBinding = ItemCitaBinding.bind(itemView)

        fun bin(cita: Cita){
            binding.tvPaciente.text = cita.nombrePaciente
            binding.tvFecha.text = cita.fechaHoraCita
            binding.tvMotivo.text = cita.motivo
            binding.tvPrecio.text = "S/ ${cita.precioCita}"

            itemView.setOnClickListener {
                callback(cita)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitaAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cita,parent,false)
        return CitaAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return citas.size
    }

    override fun onBindViewHolder(holder: CitaAdapterViewHolder, position: Int) {
        val cita : Cita = citas[position]
        holder.bin( cita )
    }

    fun updateCitas( citas: MutableList<Cita> = mutableListOf() ){
        this.citas = citas
        notifyDataSetChanged()
    }
}