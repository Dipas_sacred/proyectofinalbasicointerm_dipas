package com.desa02.proyectfinal.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import com.desa02.proyectfinal.R
import com.desa02.proyectfinal.database.AppDataBase
import com.desa02.proyectfinal.databinding.ActivityRegistroBinding
import com.desa02.proyectfinal.model.Cita
import java.util.concurrent.Executors

class RegistroActivity : AppCompatActivity() {

    lateinit var binding: ActivityRegistroBinding
    private val appDataBase: AppDataBase by lazy {
        AppDataBase.obtenerInstancia(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegistro.setOnClickListener {

            var nombre = binding.edtNombrePaciente.text.toString()
            var fechaHora = binding.edtFechaHora.text.toString()
            var precio: Double?= binding.edtPrecio.text.toString().toDoubleOrNull()
            var motivo = binding.edtMotivo.text.toString()

            if(nombre.isEmpty()){
                Toast.makeText(this,"Ingresar el nombre", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(fechaHora.isEmpty()){
                Toast.makeText(this,"Ingresar la fecha y hora", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(precio == null){
                Toast.makeText(this,"Ingresar el precio", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(motivo.isEmpty()){
                Toast.makeText(this,"Ingresar el motivo", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val cita = Cita(0,nombre,motivo,precio,fechaHora)

            Executors.newSingleThreadExecutor().execute {
                appDataBase.citaDao().insertar(cita)

                runOnUiThread(){
                    Toast.makeText(this,"Registro correcto.", Toast.LENGTH_SHORT).show()
                    irACitas()
                }
            }
        }

    }

    private fun irACitas(  ) {
        val intent = Intent(this, CitaActivity::class.java)
        startActivity(intent)
    }
}
