package com.desa02.proyectfinal.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.desa02.proyectfinal.database.AppDataBase
import com.desa02.proyectfinal.databinding.ActivityLoginBinding
import com.desa02.proyectfinal.model.Cita
import java.util.concurrent.Executors

class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding
    val myPreferences = "PREFERENCIA_LOGIN"
    val keyUsuario = "KEY_USUARIO"
    val keyClave = "KEY_CLAVE"
    val keyEstado = "KEY_ESTADO"

    private val appDataBase: AppDataBase by lazy {
        AppDataBase.obtenerInstancia(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        binding.btnIngresar.setOnClickListener {
            autenticar()
        }

        binding.btnRegister.setOnClickListener {
            irARegistroPrincipal()
        }

    }

    private fun init() {
        recuperarPreferenciaLogin()
    }


    private fun recuperarPreferenciaLogin() {
        var preferences= getSharedPreferences(myPreferences,0)
        val correo =  preferences.getString(keyUsuario,"")
        val clave =  preferences.getString(keyClave,"")
        val estado = preferences.getInt(keyEstado,-1)

        binding.edtUsuario.setText(correo)
        binding.edtPassword.setText(clave)
        binding.cbRemember.isChecked = estado==1

    }


    private fun guardarPreferenciaLogin(usuario: String, contrasenia: String, estado: Int) {
        getSharedPreferences(myPreferences,0).edit().apply(){
            putString(keyUsuario,usuario)
            putString(keyClave,contrasenia)
            putInt(keyEstado, estado)
            apply()
        }
    }

    private fun autenticar() {

        val usuario = binding.edtUsuario.text.toString()
        val contrasenia = binding.edtPassword.text.toString()
/*
        if( binding.cbRemember.isChecked )
            guardarPreferenciaLogin( usuario,contrasenia,1)
        else
            guardarPreferenciaLogin("","",-1)*/


        Executors.newSingleThreadExecutor().execute {

            val resultado = appDataBase.usuarioDao().autenticar( usuario, contrasenia )

            if( resultado == 1 ){
                if(binding.cbRemember.isChecked)
                    guardarPreferenciaLogin( usuario,contrasenia,1)
                else
                    guardarPreferenciaLogin("","",-1)

                runOnUiThread {
                    Toast.makeText(this,"Credenciales Correctas", Toast.LENGTH_SHORT).show()
                    irACitas()
                }

            }
            else{
                runOnUiThread {
                    Toast.makeText(this,"Credenciales invalidas", Toast.LENGTH_SHORT).show()
                }

            }
        }

    }

    private fun irARegistroPrincipal() {
        val intent = Intent(this, RegistroActivity::class.java)
        startActivity(intent)
    }

    private fun irACitas(  ) {
        val intent = Intent(this, CitaActivity::class.java)
        startActivity(intent)
    }
}
