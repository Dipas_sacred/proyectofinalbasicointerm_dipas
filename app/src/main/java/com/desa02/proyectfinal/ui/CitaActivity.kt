package com.desa02.proyectfinal.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.desa02.proyectfinal.R
import com.desa02.proyectfinal.adapter.CitaAdapter
import com.desa02.proyectfinal.database.AppDataBase
import com.desa02.proyectfinal.databinding.ActivityCitaBinding
import com.desa02.proyectfinal.model.Cita
import java.util.concurrent.Executors

class CitaActivity : AppCompatActivity() {
    private var citas = mutableListOf<Cita>()
    lateinit var binding: ActivityCitaBinding
    private val appDataBase: AppDataBase by lazy {
        AppDataBase.obtenerInstancia(this)
    }
    private val adapter by lazy {
        CitaAdapter(){
            val bundle = Bundle().apply {
                putSerializable("CITA", it)
            }
            val intent = Intent(this, DetalleCitaActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCitaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupAdapter()
        loadDB()

    }

    private fun setupAdapter() {
        binding.rvCita.adapter = adapter
        binding.rvCita.layoutManager = LinearLayoutManager(this)
    }

    private fun loadDB() {

        binding.progress.visibility = View.VISIBLE

        Executors.newSingleThreadExecutor().execute {
            citas = appDataBase.citaDao().listarCitas()
            adapter.updateCitas( citas )

            binding.progress.visibility = View.INVISIBLE
        }
    }
}
