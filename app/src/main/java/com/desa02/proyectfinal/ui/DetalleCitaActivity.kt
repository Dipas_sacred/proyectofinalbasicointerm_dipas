package com.desa02.proyectfinal.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.desa02.proyectfinal.R
import com.desa02.proyectfinal.databinding.ActivityDetalleCitaBinding
import com.desa02.proyectfinal.model.Cita

class DetalleCitaActivity : AppCompatActivity() {

    lateinit var binding: ActivityDetalleCitaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetalleCitaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle?=intent.extras
        bundle?.let {
            val cita = it.getSerializable("CITA") as Cita

            binding.tvPaciente.text = cita.nombrePaciente
            binding.tvFecha.text = cita.fechaHoraCita
            binding.tvMotivo.text = cita.motivo
            binding.tvPrecio.text =  "S/ ${cita.precioCita}"
        }


    }
}
