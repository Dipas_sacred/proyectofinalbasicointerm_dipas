package com.desa02.proyectfinal.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.desa02.proyectfinal.model.Cita

@Dao
interface CitaDao {
    @Insert
    fun insertar(cita: Cita)

    @Query("select * from tablaCita")
    fun listarCitas():MutableList<Cita>
}