package com.desa02.proyectfinal.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.desa02.proyectfinal.model.Cita
import com.desa02.proyectfinal.model.Usuario
import java.util.concurrent.Executors

@Database(
    entities = [Usuario::class,Cita::class],
    version = 1,
    exportSchema = false
)
abstract class AppDataBase: RoomDatabase() {
    abstract fun usuarioDao():UsuarioDao
    abstract fun citaDao():CitaDao

    companion object { // similar a un static
        private var instancia: AppDataBase? = null
        fun obtenerInstancia(context: Context): AppDataBase {
            if (instancia == null) {
                instancia = Room.databaseBuilder(
                    context, AppDataBase::class.java,
                    "BDCitas"
                ).addCallback(object : RoomDatabase.Callback() {
                    //lo que se puede hacer cuando se cree la base de datos.
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Executors.newSingleThreadExecutor().execute {
                            val usuario = mutableListOf<Usuario>()
                            usuario.add(Usuario(0,"mdipas","123456"))
                            usuario.add(Usuario(0,"jledesma","123456"))
                            instancia?.usuarioDao()?.insertar(usuario)
                        }
                    }
                    // end
                })
                    .build()
            }
            return instancia as AppDataBase
        }
    }
}