package com.desa02.proyectfinal.database

import androidx.room.*
import com.desa02.proyectfinal.model.Usuario

@Dao
interface UsuarioDao {
    @Insert
    fun insertar(usuario: MutableList<Usuario>)

    @Update
    fun actualizar(usuario: Usuario)

    @Delete
    fun eliminar(usuario: Usuario)

    @Query("Select*from tablaUsuario")
    fun obtenerUsuarios():List<Usuario>

    @Query("Select*from tablaUsuario where codigo=:codigoInput")
    fun obtenerUsuariosPorId(codigoInput:Int):List<Usuario>

    @Query("Select count(*) from tablaUsuario where usuario =:correoInput and clave=:claveInput ")
    fun autenticar(correoInput : String, claveInput:String):Int

}