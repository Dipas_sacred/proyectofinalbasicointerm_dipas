package com.desa02.proyectfinal.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "tablaUsuario")
data class Usuario( @PrimaryKey(autoGenerate = true)
                   @NonNull
                   @ColumnInfo(name = "codigo")
                   val codigo:Int,
                   @ColumnInfo(name = "usuario")
                   val correo:String,
                   @ColumnInfo(name = "clave")
                   val clave:String){
}
@Entity(tableName = "tablaCita")
data class Cita(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int,
    @ColumnInfo(name = "nombrePaciente")
    val nombrePaciente: String,
    @ColumnInfo(name = "motivo")
    val motivo:String,
    @ColumnInfo(name = "precioCita")
    val precioCita:Double,
    @ColumnInfo(name = "fechaHoraCita")
    val fechaHoraCita:String): Serializable {

}